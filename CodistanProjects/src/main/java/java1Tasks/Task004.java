package java1Tasks;

public class Task004 {

	class Calculator {

		public void add(int i, int j) {
			System.out.println(i + j);
		}

		public void add(int i, int j, int k) {
			System.out.println(i + j + k);
		}
	}

	public class OverLoading {
		public void main(String[] args) {
						Calculator calc = new Calculator();
						calc.add(10, 10);
						calc.add(4, 5, 6);
		}
			
		}

}
