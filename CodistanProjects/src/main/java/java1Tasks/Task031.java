package java1Tasks;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class Task031 {

	public static void main(String[] args)
	{
		try(BufferedReader br = new BufferedReader(new FileReader("d:\\myfile.txt")))
		{
			String str;
			while((str = br.readLine()) != null)
			{
				System.out.println(str);
			}
		}
		catch(IOException ie)
		{  
			System.out.println("exception");  
		}
	}
}
