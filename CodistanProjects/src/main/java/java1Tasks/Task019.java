package java1Tasks;

import java.util.Iterator;
import java.util.LinkedList;



public class Task019 {
	
	public static void main(String args[]){

	 LinkedList<String> list=new LinkedList<String>();

     //Adding elements to the Linked list
     list.add("Steve");
     list.add("Carl");
     list.add("Raj");


     //Iterating LinkedList
     Iterator<String> iterator=list.iterator();
     while(iterator.hasNext()){
       System.out.println(iterator.next());
     }
	}
}