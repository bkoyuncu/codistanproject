package java1Tasks;

public class Task016 {

	class Animal {
		public void sound() {
			System.out.println("Animal sound");
		}
	}

	public class Task016a extends Animal {
		public void main(String[] args) {
			Animal obj = new Task016a();
			obj.sound();

		}

}
}