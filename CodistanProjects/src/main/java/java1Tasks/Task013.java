package java1Tasks;

public abstract class Task013 {

	public abstract void animalSound();

	public void sleep() {
		System.out.println("Zzz");
	}
}

class Bear extends Task013 {
	public void animalSound() {
		System.out.println("The bear says: wee wee");
	}
}

class MyMainClass {
	public static void main(String[] args) {
		Bear myBear = new Bear();
		myBear.animalSound();
		myBear.sleep();

	}

}
