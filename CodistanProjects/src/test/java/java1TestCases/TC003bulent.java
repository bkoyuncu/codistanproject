package java1TestCases;

import org.testng.Assert;
import org.testng.annotations.Test;

public class TC003bulent {
	
	
	@Test
	public void tc003() {
		
		int a = 55;
		int b =66;
		
		int swap = a;
		a=b;
		b=swap;
		
		System.out.println("Value of a: " + a);
		System.out.println("Value of b: " + b);
		
		Assert.assertEquals(a, 66);
		Assert.assertEquals(b, 55);
		
		
		
	}

}
