package java1TestCases;

import static org.testng.Assert.assertEquals;

import java.util.Arrays;

import org.testng.Assert;
import org.testng.annotations.Test;

public class TC005bulent {

	@Test
	public void tc005() {

		int[] myArray = new int[10];
		myArray[0] = 3;
		myArray[1] = 5;
		myArray[2] = 1;
		myArray[3] = 7;
		myArray[4] = 9;
		System.out.println("Array : " + Arrays.toString(myArray));

		int count = 0;
		for (int i = 0; i < myArray.length; i++) {

			if (myArray[i] != 0) {

				count++;
			}

		}
		int[] result = new int[count];
		for (int i = 0; i < myArray.length; i++) {
			if (myArray[i] != 0) {
				result[i] = myArray[i];

			}
		}

		System.out.println("Array after removing zeros : " + Arrays.toString(result));

		int[] reverse = new int[result.length];
		for (int i = 0; i < result.length; i++) {
			reverse[i] = result[result.length - i - 1];

		}
		System.out.println("Array after reverse: " + Arrays.toString(reverse));

		int[] expected = { 9, 7, 1, 5, 3 };
		
		//Verify
		assertEquals(reverse, expected);


}
}