package java1TestCases;

import org.testng.Assert;
import org.testng.annotations.Test;

public class TC015bulent {
	
	@Test
	public void tc015() {
		
		String str = "Hello World";
		
		String upCase = str.toUpperCase();
		
		System.out.println(upCase);
		
		String expected = "HELLO WORLD";
		
		Assert.assertEquals(upCase, expected);
	}
}
