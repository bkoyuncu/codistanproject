package java1TestCases;

import org.testng.Assert;
import org.testng.annotations.Test;

public class TC007bulent {
	
	@Test
	public void tc007() {
		
		String firstName = "John";
		String lastName = "Doe";
		String fullName = firstName + " " + lastName;
		
		System.out.println("Actual result is: " + fullName);
		System.out.println("Expected result is: " + "John Doe");
		
		Assert.assertEquals(fullName, "John Doe");
	}

}
