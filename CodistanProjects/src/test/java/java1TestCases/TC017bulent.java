package java1TestCases;

import org.testng.Assert;
import org.testng.annotations.Test;

public class TC017bulent {
	
	@Test
	public void tc017() {
		
		StringBuilder strBuilder = new StringBuilder("Hello World");
		
		strBuilder.reverse();
		String str = strBuilder.toString();
		System.out.println(str);
		
		Assert.assertEquals(str, "dlroW olleH");
		
	}

}
